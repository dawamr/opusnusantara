<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use DB;
use Exporter;
use Excel;


class OrganizerLombaDownload extends Controller
{

    public function bukuacara_view($id){

        $data['lomba'] = \App\Lomba::find($id);
        return view('organizer.download.bukuacara')->with('data',$data);

    }

    public function bukuacara_download(Request $request, $id){
        $data['lomba'] = \App\Lomba::find($id);
        $data['kategori'] = \App\LombaKategori::find($request->kategori_id);

        if($request->kategori_id == 'all'){
            $data['kat'] = \App\LombaKategori::where('lomba_id', $id)->get();
            // dd($data['kat']);
            if($request->output == 'pdf'){
                $pdf = PDF::loadView('pdf.bukuacara_all', $data)->setPaper('a4', 'landscape')->setWarnings(false);

                return $pdf->download('bukuacara_all.pdf');
            }

            if($request->output == 'xlsx'){
                return \Excel::download(new \App\Exports\bukuAcaraAll_xls ($data), 'Buku Acara All.xlsx');
            }
        }

        if($request->output == 'pdf'){
            $pdf = PDF::loadView('pdf.bukuacara', $data)->setPaper('a4', 'landscape')->setWarnings(false);

            return $pdf->download($data['kategori']->name.'.pdf');
        }

        if($request->output == 'xlsx'){
          return \Excel::download(new \App\Exports\bukuAcara_xls ($data), 'Buku Acara '.$data['kategori']->name.'.xlsx');
        }

    }

    public function bukuacara_xlsx($data){

    }

    public function bukuacara_pdf($data){

    }

    public function guidebook_view($id){

        $data['lomba'] = \App\Lomba::find($id);
        return view('organizer.download.guidebook')->with('data',$data);

    }

    public function guidebook_proses(Request $request,$id){

        $data['lomba'] = \App\Lomba::find($id);
        $data['kategori'] = \App\LombaKategori::find($request->kategori_id);

        $pdf = PDF::loadView('pdf.guidebook', $data)->setPaper('a4', 'portrait')->setWarnings(false);

        return $pdf->download($data['kategori']->name.'.pdf');
    }

    public function rekap_view($id){

        $data['lomba'] = \App\Lomba::find($id);
        return view('organizer.download.rekap')->with('data',$data);

    }

    public function rekap_proses(Request $request, $id){

        $data['lomba'] = \App\Lomba::find($id);
        // dd($request);
        //kalau all kategori
        if($request->kategori_id == 'all'){
            //$data['kategori'] = \App\LombaKategori::where('lomba_id',$id)->get();
            if($request->output == 'pdf'){
                $pdf = PDF::loadView('pdf.rekap_all', $data)->setPaper('a4', 'landscape')->setWarnings(false);

                return $pdf->download('rekap_all.pdf');
            }

            if($request->output == 'xlsx'){
                return \Excel::download(new \App\Exports\rekapPendaftaran_xls ($data), 'Rekap Pendaftaran '.$data['lomba']->name.'.xlsx');
            }
        }
    }

    public function rekap_peserta_view($id){

        $data['lomba'] = \App\Lomba::find($id);
        return view('organizer.download.rekap_peserta')->with('data',$data);

    }

    public function rekap_peserta_proses(Request $request, $id){

        $data['lomba'] = \App\Lomba::find($id);
        // dd($request);
        //kalau all kategori
        if($request->kategori_id == 'all'){
            //$data['kategori'] = \App\LombaKategori::where('lomba_id',$id)->get();
            if($request->output == 'pdf'){
                $pdf = PDF::loadView('pdf.rekap_peserta_all', $data)->setPaper('a4', 'landscape')->setWarnings(false);

                return $pdf->download('rekap_peserta_all.pdf');
            }

            if($request->output == 'xlsx'){
                  return \Excel::download(new \App\Exports\rekapPeserta_xls ($data), 'Rekap Peserta '.$data['lomba']->name.'.xlsx');

            }
        }

        //kalau kategori sendiri-sendiri
        // $data['kategori'] = \App\LombaKategori::find($request->kategori_id);

        // if($request->output == 'pdf'){
        //     $pdf = PDF::loadView('pdf.rekap', $data)->setPaper('a4', 'landscape')->setWarnings(false);

        //     return $pdf->download($data['kategori']->name.'.pdf');
        // }

        // if($request->output == 'xlsx'){

        // }

    }

    public function komentar_view($id){

        $data['lomba'] = \App\Lomba::find($id);
        return view('organizer.download.komentar')->with('data',$data);

    }

    public function komentar_proses(Request $request, $id){
        $data['lomba'] = \App\Lomba::find($id);
        $data['kategori'] = \App\LombaKategori::find($request->kategori_id);
        $data['keterangan'] = $request->keterangan;

        if($request->kategori_id == 'all'){
            $data['kat'] = \App\LombaKategori::where('lomba_id', $id)->get();
            // dd($data['kat']);
            if($request->output == 'pdf'){
                $pdf = PDF::loadView('pdf.komentar_all', $data)->setPaper('a4', 'portrait')->setWarnings(false);

                return $pdf->download('komentar_all.pdf');
            }


        }

        if($request->output == 'pdf'){
            $pdf = PDF::loadView('pdf.komentar', $data)->setPaper('a4', 'landscape')->setWarnings(false);

            return $pdf->download($data['kategori']->name.'.pdf');
        }



    }

    public function penilaian_view($id){

        $data['lomba'] = \App\Lomba::find($id);
        return view('organizer.download.penilaian')->with('data',$data);

    }

    public function penilaian_proses(Request $request, $id){
        $data['lomba'] = \App\Lomba::find($id);
        $data['kategori'] = \App\LombaKategori::find($request->kategori_id);

        return view('organizer.download.penilaian_proses')->with('data',$data);
    }

    public function penilaian_simpan(Request $request, $id){
      // $penilaian = \App\LombakuPeserta::find($id);

        $peserta_id = $request->id;
        $nilai1 = $request->nilai1;
        $nilai2 = $request->nilai2;
        $nilai3 = $request->nilai3;
        $juara = $request->juara;

        for ($i=0; $i <sizeof($peserta_id) ; $i++) {
            $peserta = \App\LombakuPeserta::find($peserta_id[$i]);
            $peserta->nilai1 = $nilai1[$i];
            $peserta->nilai2 = $nilai2[$i];
            $peserta->nilai3 = $nilai3[$i];
            $rata2 = ($peserta->nilai1 + $peserta->nilai2 + $peserta->nilai3)/3;
            $peserta->rata2 = $rata2;
            $peserta->juara =$juara[$i];
            $peserta->save();
        }

        return redirect('/organizer/lomba/'.$id.'/download/penilaian/list/?kategori_id='.$request->kategori_id);
    }

    public function hasil_view($id){
        $data['lomba'] = \App\Lomba::find($id);
        return view('organizer.download.hasil')->with('data',$data);
    }

    public function hasil_proses(Request $request, $id){
      // $peserta = \App\LombakuPeserta::where('kategori_id', $request->id)->get();
      // dd($peserta);
      $data['lomba'] = \App\LombakuPeserta::find($id);
      $data['kategori'] = \App\LombaKategori::find($request->kategori_id);

      if($request->kategori_id == 'all'){
          $kategoris = \App\LombaKategori::where('lomba_id', $id)->get();
          // dd($kategoris);
          if($request->output == 'pdf'){
              $pdf = PDF::loadView('pdf.hasilkompetisi_all', compact('kategoris'))->setPaper('a4', 'landscape')->setWarnings(false);
              return $pdf->download('Hasil Kompetisi All.pdf');
          }

          if($request->output == 'xlsx'){
            return \Excel::download(new \App\Exports\hasilKompetisiAll_xls (compact('kategoris')), 'Hasil Kompetisi All.xlsx');
          }
          if($request->output == 'html'){
            return view('pdf.hasilkompetisi_all')->with('kategoris', $kategoris);

          }
      }

      if($request->output == 'pdf'){
          $pdf = PDF::loadView('pdf.hasilkompetisi', $data)->setPaper('a4', 'portrait')->setWarnings(false);

          return $pdf->download('Hasil Kategori'.$data['kategori']->name.'.pdf');

      }
      if($request->output == 'html'){
        return view('pdf.hasilkompetisi')->with('kategori', $data['kategori']);

      }
        if($request->output == 'xlsx'){
          // return $lombaku;
        return \Excel::download(new \App\Exports\hasilExport_xls ($data), 'Hasil Kategori '.$data['kategori']->name.'.xlsx');
        }
    }

    public function sertifikat_view($id){
            $data['lomba'] = \App\Lomba::find($id);
            return view('organizer.download.sertifikat')->with('data',$data);
        }
        public function sertifikat_proses(Request $request, $id){
          // $peserta = \App\LombakuPeserta::where('kategori_id', $request->id)->get();
          // dd($peserta);
          $data['lomba'] = \App\LombakuPeserta::find($id);
          $data['kategori'] = \App\LombaKategori::find($request->kategori_id);
          // dd($data['kategori']);
          if($request->kategori_id == 'all'){
              $kategori = \App\LombaKategori::where('lomba_id', $id)->get();
              // dd($kategoris);
              if($request->output == 'pdf'){
                  $pdf = PDF::loadView('pdf.sertifikat_all', compact('kategori'))->setPaper('a4', 'landscape')->setWarnings(false);
                  return $pdf->download('Sertifikat Peserta All.pdf');
              }

              if($request->output == 'html'){
                return view('pdf.sertifikat_all')->with('kategori', $kategori);

              }
          }

          if($request->output == 'pdf'){
              $pdf = PDF::loadView('pdf.sertifikat', $data)->setPaper('a4', 'landscape')->setWarnings(false);

              return $pdf->download('Sertifikat Kategori '.$data['kategori']->name.'.pdf');

          }
          if($request->output == 'html'){
            return view('pdf.sertifikat')->with('kategori', $data['kategori']);

          }
        }

}
