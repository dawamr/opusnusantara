<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JuriLombaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lomba = \App\Lomba::find($id);

        // dd($lomba);
        return view('juri.lomba.show')->with('lomba', $lomba);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function nilai(Request $request, $id){
        $lomba = \App\Lomba::find($id);
        $lomba->kategori_id = $request->kategori_id;
        // dd($lomba);
        return view('juri.lomba.nilai')->with('lomba', $lomba);
    }

    public function penilaian(Request $request, $id){
        $lomba = \App\Lomba::find($id);
        $lomba->kategori_id = $request->kategori_id;
        $lomba->peserta_id = $request->peserta_id;
        // dd($lomba);
        return view('juri.lomba.penilaian')->with('lomba', $lomba);
    }

    public function simpan(Request $request, $id){

        $peserta_id = $request->id;
        $nilai1 = $request->nilai1;
        $nilai2 = $request->nilai2;
        $nilai3 = $request->nilai3;
        $juara = $request->juara;

        for ($i=0; $i <sizeof($peserta_id) ; $i++) {
            $peserta = \App\LombakuPeserta::find($peserta_id[$i]);
            if($request->nilai1 != null){
                $peserta->nilai1 = $nilai1[$i];
            }
            if($request->nilai2 != null){
                $peserta->nilai2 = $nilai2[$i];
            }
            if($request->nilai3 != null){
                $peserta->nilai3 = $nilai3[$i];
            }
           
            $rata2 = ($peserta->nilai1 + $peserta->nilai2 + $peserta->nilai3)/3;
            $peserta->rata2 = $rata2;
            $peserta->juara =$juara[$i];
            $peserta->save();
        }

        // return redirect('/organizer/lomba/'.$id.'/download/penilaian/list/?kategori_id='.$request->kategori_id);
        
        return redirect('/juri/lomba/' . $id . "/nilai/?kategori_id=" . $request->kategori_id);
    }
}
