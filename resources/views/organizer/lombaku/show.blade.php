@extends('layouts.organizer')
@section('css')
@endsection

@section('content')
<div class="container">
    <!-- <div class="card-group mt-4">
        <div class="card col-sm-12 col-lg-2">
            <img class="img-responsive card-img-top" src="http://www.opusnusantara.com/images/Poster_KPP_2018.jpg" alt="Card image cap">
        </div>
        <div class="card col-sm-12 col-lg-10">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
        </div>
    </div> -->
    <!-- end card group -->
    <br>

    <div class="card">
        <div class="card-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h6 class="h-block">
                            Data Peserta
                        <h6>
                    </div>
                  
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="container">
                <?php
                    $user = \App\User::find($lombaku->user_id);

                ?>
                <h4>Data Pendaftar/Guru</h4>
                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th>Keterangan</th>
                            <th>Data</th>
                        </tr>
                    </thead>
                    <tbody>



                        <tr>
                            <td>Nama</td>
                            <td>{{$user->name}}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{{$user->email}}</td>
                        </tr>
                        <tr>
                            <td>No HP</td>
                            <td>{{$user->nohp}}</td>
                        </tr>

                 



                    </tbody>
                </table>

                <br />
                <h4>Data Peserta</h4>

                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th scope="col">Nama</th>
                            <th>Tanggal Lahir</th>
                            <th>Kategori</th>
                            <th>Lagu</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            // dd($lomba->lombaku);
                            $pesertas = $lombaku->peserta;
                            // dd(sizeof($lombaku[0]->peserta))
                        ?>

                        @foreach($pesertas as $peserta)

                        <?php

                        ?>
                        <tr>

                            <td>{{$peserta->nama}}</td>
                            <td>{{$peserta->tanggal_lahir}}</td>
                            <?php
                                $kategori = \App\LombaKategori::find($peserta->kategori_id);
                                if($kategori->song_type == 'bebas'){
                                    $song = $peserta->song1;
                                } else {
                                    $song = $kategori['song'.$peserta->song1];
                                }
                                // dd($song);
                            ?>
                            <td>{{$kategori->name}}</td>
                            <td>{{$song}}</td>
                            
                           
                        </tr>
                        @endforeach
                 



                    </tbody>
                </table>
                <br />

                <h4>Total Pembayaran:</h4>
                <strong><p>Rp {{number_format($lombaku->total_biaya,2)}}</p></strong>
                <hr>
                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th>Keterangan</th>
                            <th>Data</th>
                        </tr>
                    </thead>
                    <tbody>



                        <tr>
                            <td>Tanggal Transfer</td>
                            <td>{{$lombaku->tanggal_bayar}}</td>
                        </tr>
                        <tr>
                            <td>Bank Asal</td>
                            <td>{{$lombaku->bank_bayar}}</td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td>{{$lombaku->nama_bayar}}</td>
                        </tr>

                 



                    </tbody>
                </table>

                <div class="row">
                    <div class="col">

                        <a href="#" class="hapus-pendaftaran">
                            <button type="button" class="btn btn-danger waves-effect waves-light">Hapus Pendaftaran</button>
                        </a>

                    </div>
                    <div class="col" align="right">
                        @if($lombaku->status != 200)
                        <a href="#" class="confirm-pendaftaran" >

                            <button type="button" class="btn btn-primary waves-effect waves-light">Konfirmasi Pembayaran Ini</button>
                        </a>
                        @endif
                        @if($lombaku->status == 200)
                        <a href="#" class="confirm-pendaftaran" >

                            <button type="button" class="btn btn-success waves-effect waves-light">LUNAS</button>
                        </a>
                        @endif
                    </div>
                </div>
               


            </div>
        </div>
    </div>
    <br />

   
</div>
<!-- end container -->
@endsection

@section('js')
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
$('#datapembayaran').DataTable();
$('#datapeserta').DataTable();
$('.confirm-pendaftaran').click(function(){
    // alert("hello");
    var kategoriId = $(this).attr('kategori-id');
    var kategoriName = $(this).attr('kategori-nama');
    var lombaId = $(this).attr('lomba-id');

    if(confirm("Anda Yakin Ingin Konfirmasi Pendaftaran Ini / Email Ulang Pembayaran Sukses Peserta?")){
      axios.post('/organizer/lomba/{{$lombaku->lomba_id}}/lombaku/{{$lombaku->id}}/confirm')
        .then(function (response) {
          console.log(response);
          window.location.reload();
        })
        .catch(function(error){
            console.log(error);
        });
    }

});
$('.hapus-pendaftaran').click(function(){
    // alert("hello");
    var kategoriId = $(this).attr('kategori-id');
    var kategoriName = $(this).attr('kategori-nama');
    var lombaId = $(this).attr('lomba-id');

    if(confirm("Anda Yakin Ingin hapus Pembayaran Ini ?")){
      axios.post('/organizer/lomba/{{$lombaku->lomba_id}}/lombaku/{{$lombaku->id}}/delete')
        .then(function (response) {
          console.log(response);
          window.location = '/organizer/lomba/{{$lombaku->lomba_id}}';
        })
        .catch(function(error){
            console.log(error);
        });
    }

});
</script>
@endsection
