<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
.page-break {
    page-break-after: always;
}
</style>

<?php $i=0;?>
@foreach($kat as $kategori)
<img src="{{ base_path() }}/public/image/logo.png" height="35px" align="left">
<br />
@if($i==0)
<h1 align="center">Buku Acara</h1>
<h2 align="center">{{$lomba->name}}</h2>

@endif
<br />
<?php
    $pesertas = \App\LombakuPeserta::where('kategori_id', $kategori->id)->orderBy('no_undian', 'asc')->get();
    $jumlah_peserta = sizeof($pesertas);
    $i++;
?>

<h3>{{$kategori->name}} ({{$jumlah_peserta}} peserta)</h3>
<table class="table table-bordered">
<tbody>
  <tr>
    <th style="width: 50px;">No</th>
    <th style="width: 250px;">Nama</th>
    <th style="width: 100px;">TTL</th>
    <th style="width: 200px;">Sekolah</th>
    <th style="width: 50px;">Kelas</th>
    <th style="width: 350px;">Lagu</th>
  </tr>
  
  @foreach($pesertas as $peserta)

  <?php
    $lombaku = $peserta->lombaku;
    if($lombaku->status == '200'){
        $status = "Lunas";
    }elseif($lombaku->bank_bayar == null){
        $status = "Belum Konfirmasi";
    }elseif($lombaku->bank_bayar && $lombaku->status != '200'){
        $status = "Menunggu Konfirmasi Admin";
    }
  ?>
  <tr>
    <td>{{$peserta->no_undian}}</td>
    <td style="width: 165px">{{$peserta->nama}}</td>
    <td style="width: 50px">{{$peserta->tanggal_lahir}}</td>
    <td style="width: 165px">{{$peserta->sekolah_nama}}</td>
    <td style="width: 50px">{{$peserta->sekolah_tingkat}}</td>
    <td style="width: 200px">
        <?php
            if($kategori->song_type == 'bebas'){
                $song = $peserta->song1;
            } else {
                $song = $kategori['song'.$peserta->song1];
            }
        ?>

        {{$song}}
    </td> 
  </tr>
  @endforeach
  </tbody> 
</table>
<br />
<div class="page-break"></div>
@endforeach