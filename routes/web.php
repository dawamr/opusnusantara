<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/news', function(){
    return view('pengumuman');

});

Route::get('/news/{id}','NewsController@show');

Route::get('/competition', function () {
    return view('competition');
});
Route::get('/gallery', function () {
    return view('gallery');
});
Route::get('/gallery/{id}','LombaGalleryController@show');
Route::get('/education', function () {
    return view('education');
});

Route::resource('lomba', 'LombaController');

// Daftar
Route::get('daftar', function() {
  return view('daftar.daftar');
});


Route::get('daftar/create', function() {
  return view('daftar.create');
});

Route::get('daftar/add', function() {
  return view('daftar.add');
});

Route::get('daftar/bayar', function() {
  return view('daftar.bayar');
});

Auth::routes();

Route::get('/pengumuman', function () {
    return view('pengumuman');
});

Route::get('/contact', function () {
    return view('contact');
});


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', function(){
    Auth::logout();
    return redirect('/');
});

Route::group(['prefix'=>'admin', 'middleware'=>['auth','role:admin']], function () {
    // Route diisi disini...
    Route::get('/', function(){
        return view('admin.index');
    });

});

Route::group(['prefix'=>'organizer', 'middleware'=>['auth','role:organizer']], function () {
    // Route diisi disini...
    Route::get('/', function(){
        return view('organizer.index');
    });

    // Route::get('lomba/{id}/delete', "OrganizerLombaController@delete");
    // Route::get('lomba/{id}/delete', "OrganizerLombaController@delete");

    Route::resource('lomba', 'OrganizerLombaController');
    Route::resource('lomba/{lomba_id}/kategori', 'OrganizerLombaKategoriController');

    // Route::post('/organizer/lomba', "kontenController@add");
    Route::resource('/lomba/{lomba_id}/konten','LombaKontenController');
    Route::resource('/news','NewsController');
    Route::delete('/news','NewsController@destroy');
    Route::put('/news','NewsController@update');

    Route::get('lomba/{lomba_id}/lombaku/{lombaku_id}','OrganizerLombakuController@show');
    Route::post('lomba/{lomba_id}/lombaku/{lombaku_id}/confirm','OrganizerLombakuController@confirm');
    Route::post('lomba/{lomba_id}/lombaku/{lombaku_id}/delete','OrganizerLombakuController@delete');

    Route::get('lomba/{lomba_id}/peserta/{id}','OrganizerPesertaController@show');
    Route::get('lomba/{lomba_id}/peserta/{id}/edit','OrganizerPesertaController@edit');
    Route::put('lomba/{lomba_id}/peserta/{id}/','OrganizerPesertaController@update');
    Route::post('lomba/{lomba_id}/peserta/{id}/reset_undian','OrganizerPesertaController@reset_undian');

    //download laporan
    Route::get('lomba/{lomba_id}/download/bukuacara','OrganizerLombaDownload@bukuacara_view');
    Route::post('lomba/{lomba_id}/download/bukuacara','OrganizerLombaDownload@bukuacara_download');
    Route::get('lomba/{lomba_id}/download/guidebook','OrganizerLombaDownload@guidebook_view');
    Route::post('lomba/{lomba_id}/download/guidebook','OrganizerLombaDownload@guidebook_proses');
    Route::get('lomba/{lomba_id}/download/rekap','OrganizerLombaDownload@rekap_view');
    Route::post('lomba/{lomba_id}/download/rekap','OrganizerLombaDownload@rekap_proses');

    Route::get('lomba/{lomba_id}/download/rekap_peserta','OrganizerLombaDownload@rekap_peserta_view');
    Route::post('lomba/{lomba_id}/download/rekap_peserta','OrganizerLombaDownload@rekap_peserta_proses');

    Route::get('lomba/{lomba_id}/download/komentar','OrganizerLombaDownload@komentar_view');
    Route::post('lomba/{lomba_id}/download/komentar','OrganizerLombaDownload@komentar_proses');
    // Route::post('lomba/{lomba_id}/peserta/{id}','OrganizerPesertaController@update');
    // Route::resource('lomba/{lomba_id}/peserta/','OrganizerPesertaController');
    Route::get('lomba/{lomba_id}/download/penilaian', 'OrganizerLombaDownload@penilaian_view');
    Route::get('lomba/{lomba_id}/download/penilaian/list', 'OrganizerLombaDownload@penilaian_proses');
    Route::post('lomba/{lomba_id}/download/penilaian_simpan', 'OrganizerLombaDownload@penilaian_simpan');
    Route::get('lomba/{lomba_id}/download/hasil','OrganizerLombaDownload@hasil_view');
    Route::post('lomba/{lomba_id}/download/hasil','OrganizerLombaDownload@hasil_proses');
    Route::get('lomba/{lomba_id}/download/sertifikat','OrganizerLombaDownload@sertifikat_view');
    Route::post('lomba/{lomba_id}/download/sertifikat','OrganizerLombaDownload@sertifikat_proses');

    Route::get('gallery', 'OrganizerImagesController@show');
    Route::get('gallery/{id}', 'OrganizerImagesController@index');
    Route::post('gallery/storeImage/{id}','OrganizerImagesController@storeImage');
    Route::get('gallery/allImages','OrganizerImagesController@allImages')->name('allImages');
    Route::post('gallery/deleteImage/{id}','OrganizerImagesController@deleteImage');
    Route::post('gallery/deleteImages','OrganizerImagesController@deleteImages');
});

Route::group(['prefix'=>'guru', 'middleware'=>['auth','role:guru']], function () {
    // Route diisi disini...
    Route::get('/', function(){
        return view('guru.index');
    });
    // Route::get('peserta','GuruController');


});

Route::group(['prefix'=>'peserta', 'middleware'=>['auth','role:peserta']], function () {
    // Route diisi disini...
    Route::get('/', function(){
        return view('peserta.index');
    });

});

Route::group(['prefix'=>'juri', 'middleware'=>['auth','role:juri']], function () {
    // Route diisi disini...
    Route::get('/', function(){
        return view('juri.index');
    });
    Route::resource('/lomba', 'JuriLombaController');
    Route::get('/lomba/{id}/nilai', 'JuriLombaController@nilai');
    Route::get('/lomba/{id}/penilaian', 'JuriLombaController@penilaian');
    Route::post('/lomba/{id}/penilaian', 'JuriLombaController@simpan');

});

Route::group(['middleware'=>['auth']], function () {
    // Route diisi disini...
    Route::resource('lombaku','LombakuController');
    Route::resource('pesertaku','PesertakuController');
    Route::resource('lombaku/{lombakuId}/peserta','LombakuPesertaController');
    Route::get('lombaku/{lombakuId}/konfirmasi','LombakuController@showKonfirmasi');
    Route::post('lombaku/{lombakuId}/konfirmasi','LombakuController@storeKonfirmasi');
    Route::get('lombaku/{lombakuId}/pembayaran','LombakuController@showPembayaran');
    Route::post('lombaku/{lombakuId}/pembayaran','LombakuController@savePembayaran');
    Route::post('lombaku/{lombaId}/undian/{pesertaId}', 'LombakuController@ambilUndian');

});

Route::get('/hello/{id}', function($id){
    return 'helo' . $id;
});
